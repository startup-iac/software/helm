#!/bin/bash

# Add Helm Repository
wget -qO - https://baltocdn.com/helm/signing.asc | sudo gpg --dearmor -o $KEYRINGS_PATH/helm-keyring.gpg
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=$KEYRINGS_PATH/helm-keyring.gpg] \
    https://baltocdn.com/helm/stable/debian/ all main" \
    | sudo tee /etc/apt/sources.list.d/helm.list

# Update Repositories
sudo apt-get update

# Install Helm
sudo apt-get install -y helm